#ifndef UTILS_HPP
#define	UTILS_HPP

#include "matrix.hpp"
#include "vector.hpp"
#include "matrix_op.hpp"

template<typename T>
T diff_max(Vector<T>& a, Vector<T>& b)
{
    if (a.get_size() != b.get_size()) {
        cerr << "Vector wrong dimensions" << endl;
        exit(2);
    }
    
    T diff = 0.;
    
    for (int i = 0; i < a.get_size(); i++)
        diff = max(diff, a(i) - b(i));
    return diff;
}

template<typename T>
void print_result(T result, unsigned precision=6) 
{
    printf("%.*f\n", precision, result);
}

template<>
void print_result<double>(double result, unsigned precision) 
{
    printf("%.*lf\n", precision, result);
}

template<>
void print_result<long double>(long double result, unsigned precision) 
{
    printf("%.*Lf\n", precision, result);
}

#endif	/* UTILS_HPP */

