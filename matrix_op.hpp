#ifndef MATRIX_OP_H
#define	MATRIX_OP_H

#include <cmath>

#include "matrix.hpp"

template<typename T>
class MatrixOp {
    
    public:
        static Vector<T> gauss(Matrix<T> a, Vector<T> b);
        static Vector<T> thomas(Vector<T> a, Vector<T> b, Vector<T> c, Vector<T> d);
        static Vector<T> thomas(Matrix<T> a, Vector<T> b);
        static Matrix<T>& invert(); 
        
    private:

};

template<typename T>
Vector<T> MatrixOp<T>::gauss(Matrix<T> a, Vector<T> b)
{   
    if (a.get_rows() != b.get_rows()) {
        cerr << "Vector wrong dimensions or vector not transposed" << endl;
        exit(2);
    }
 
    T m, s;
        
    // forward elimination
    for (int i = 0; i < a.get_cols(); i++) 
    {
        if (a(i, i) == 0) {
            int j = i + 1;
            while (a(i, i) == 0 and j < a.get_rows()) {
                a.swap_rows(i, j);
                b.swap_rows(i, j);
            }
            if (a(i, i) == 0) {
                cerr << "Matrix is degenerated" << endl;
                exit(2);
            }
        }
        for (int j = i + 1; j < a.get_rows(); j++) 
        {
            m = -a(i, j) / a(i, i);
            for (int k = i; k < a.get_cols(); k++)
                a(k, j) += m * a(k, i);
            b(j) += m * b(i);
        }
    }    
    
    Vector<T> x(b.get_size());
    x.transpose();

    // backward substitution
    for (int i = a.get_rows() - 1; i >= 0; i--)
    {
        s = b(i);
        for (int j = a.get_cols() - 1; j >= 0; j--)
            s -= a(j, i) * x(j);
        x(i) = s / a(i, i);
    }
    
    return x;
}

template<typename T>
Vector<T> MatrixOp<T>::thomas(Vector<T> a, Vector<T> b, Vector<T> c, Vector<T> d)
{
    if (b.get_size() != d.get_size()) {
        cerr << "Vector wrong dimensions" << endl;
        exit(2);
    }
    
    Vector<T> x(d.get_size());
    
    // forward sweep
    // sweep bottom diagonal
    for (int i = 0; i < d.get_size() - 1; i++) {
        d(i + 1) -= d(i) * a(i) / b(i);
        b(i + 1) -= c(i) * a(i) / b(i);
    }
    
    // sweep upper diagonal
    for (int i = d.get_size() - 2; i >= 0; i--)
        d(i) -= d(i + 1) * c(i) / b(i + 1);

    // produce solutions
    for (int i = 0; i < d.get_size(); i++)
        x(i) = d(i) / b(i);
    
    return x;
}

template<typename T>
Vector<T> MatrixOp<T>::thomas(Matrix<T> a, Vector<T> b)
{
    if (a.get_rows() != b.get_rows()) {
        cerr << "Vector wrong dimensions or vector not transposed" << endl;
        exit(2);
    }
    
    Vector<T> x(b.get_size());
    
    // forward sweep
    // sweep bottom diagonal
    for (int i = 0; i < b.get_size() - 1; i++) {
        b(i + 1) -= b(i) * a(i, i + 1) / a(i, i); // update b-vector as well
        a(i + 1, i + 1) -= a(i + 1, i) * a(i, i + 1) / a(i, i);
    }
    
    // sweep upper diagonal
    for (int i = b.get_size() - 2; i >= 0; i--)
        b(i) -= b(i + 1) * a(i + 1, i) / a(i + 1, i + 1);
    
    // produce solutionss
    for (int i = 0; i < b.get_size(); i++)
        x(i) = b(i) / a(i, i);
    
    cout << x << endl;
    return x;
}
            
#endif	/* MATRIX_OP_H */
