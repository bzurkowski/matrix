#ifndef ARRAY2D_HPP
#define	ARRAY2D_HPP

#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

// friend functions pre-declaration
template<typename T> class Array2D;
template<typename T> ostream& operator<< (ostream& out, const Array2D<T>& a);
template<typename T> Array2D<T> operator+ (const Array2D<T>& a, const Array2D<T>& b);
template<typename T> Array2D<T> operator+ (const Array2D<T>& a, const T& value);
template<typename T> Array2D<T> operator+ (const T& value, const Array2D<T>& a);
template<typename T> Array2D<T> operator* (const Array2D<T>& a, const Array2D<T>& b);
template<typename T> Array2D<T> operator* (const Array2D<T>& a, const T& value);
template<typename T> Array2D<T> operator* (const T& value, const Array2D<T>& a);  

template<typename T>
class Array2D
{
    protected:
        unsigned cols_, rows_;
        T** data_;
        
    public:
        // constructors, destructors
        Array2D(unsigned cols, unsigned rows);
        Array2D(const Array2D& a);
        ~Array2D();
                
        
        // elements access
        T& operator() (unsigned col, unsigned row);
        T get_elem(unsigned col, unsigned row) const;
        
        unsigned get_cols() const;
        unsigned get_rows() const;
        
        // operators
        Array2D<T>& operator= (const Array2D<T>& a);
        Array2D<T>& operator= (const T& value);        
        friend Array2D<T> operator+ <> (const Array2D<T>& a, const Array2D<T>& b);
        friend Array2D<T> operator+ <> (const Array2D<T>& a, const T& value);
        friend Array2D<T> operator+ <> (const T& value, const Array2D<T>& a);
        Array2D<T>& operator+= (const Array2D<T>& a);
        Array2D<T>& operator+= (const T& value);
        friend Array2D<T> operator* <> (const Array2D<T>& a, const Array2D<T>& b);
        friend Array2D<T> operator* <> (const Array2D<T>& a, const T& value);
        friend Array2D<T> operator* <> (const T& value, const Array2D<T>& a);
        Array2D<T> operator*= (const Array2D<T>& a);
        Array2D<T>& operator*= (const T& value);
        friend ostream& operator<< <> (ostream& out, const Array2D<T>& a);
        
        // operations
        Array2D<T>& transpose();
        Array2D<T>& swap_rows(unsigned  row, unsigned target_row);
        Array2D<T>& swap_cols(unsigned  col, unsigned target_col);
        
        // utils
        Array2D<T>& fill(T value);
        Array2D<T>& zeros();
        Array2D<T>& ones(); 
        Array2D<T>& load(const string& input_file_name);
        
    private:
        T** init_data(unsigned cols, unsigned rows, T initial_value);
};

template<typename T>
Array2D<T>::Array2D(unsigned cols, unsigned rows)
{
    cols_ = cols;
    rows_ = rows;
    data_ = this->init_data(cols, rows, 0.);
}

template<typename T>
Array2D<T>::Array2D(const Array2D<T>& m)
{
    cols_ = m.get_cols();
    rows_ = m.get_rows();
    data_ = this->init_data(cols_, rows_, 0.);
    
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data_[i][j] = m.get_elem(i, j);
}

template<typename T>
Array2D<T>::~Array2D()
{
    for (int i = 0; i < cols_; i++)
        delete [] data_[i];
     delete [] data_;
}

template<typename T>
T& Array2D<T>::operator() (unsigned col, unsigned row)
{
    if (col >= cols_ || row >= rows_) {
        cerr << "Array index out of bounds" << endl;
        exit(2);
    }
    return data_[col][row];
}

template<typename T>
T Array2D<T>::get_elem(unsigned col, unsigned row) const
{
    if (col >= cols_ || row >= rows_) {
        cerr << "Array index out of bounds" << endl;
        exit(2);
    }
    return data_[col][row];
}

template<typename T>
Array2D<T>& Array2D<T>::operator= (const Array2D<T>& a)
{
    if (this ==&a) {
        return *this;
    } else {
        if (a.get_cols() != cols_ || a.get_rows() != rows_) {
            this->~Array2D();
            cols_ = a.get_cols();
            rows_ = a.get_rows();
            data_ = this->init_data(cols_, rows_, 0.);
        }
        for (int i = 0; i < cols_; i++)
            for (int j = 0; j < rows_; j++)
                data_[i][j] = a.get_elem(i, j);
        return *this;
    }
}

template<typename T>
Array2D<T>& Array2D<T>::operator= (const T& value)
{
    return this->fill(value);
}

template<typename T>
Array2D<T>& Array2D<T>::operator+= (const Array2D<T>& a)
{
    if (a.get_cols() != cols_ || a.get_rows() != rows_) {
        cerr << "Wrong array dimensions" << endl;
        exit(2);
    }
    
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data_[i][j] += a.get_elem(i, j);
    return *this;
}

template<typename T>
Array2D<T>& Array2D<T>::operator+= (const T& value)
{
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data_[i][j] += value;
    return *this;
}

template<typename T>
Array2D<T> operator+ (const Array2D<T>& a, const Array2D<T>& b)
{
    Array2D<T> m(a);
    return (m += b);
}

template<typename T>
Array2D<T> operator+ (const Array2D<T>& a, const T& value)
{
    Array2D<T> m(a);
    return (m += value);
}

template<typename T>
Array2D<T> operator+ (const T& value, const Array2D<T>& a)
{
    Array2D<T> m(a);
    return (m += value);
}
   
template<typename T>
Array2D<T> Array2D<T>::operator*= (const Array2D<T>& a)
{
    if (a.get_rows() != cols_) {
        cerr << "Wrong array dimensions" << endl;
        exit(2);
    }
    
    Array2D<T> m(a.get_cols(), rows_);
    
    for (int i = 0; i < a.get_cols(); i++)
        for (int j = 0; j < rows_; j++)
            for (int k = 0; k < cols_; k++)
                m.data_[i][j] += data_[k][j] * a.get_elem(i, k);
    return m; 
}

template<typename T>
Array2D<T>& Array2D<T>::operator*= (const T& value)
{
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data_[i][j] *= value;
    return *this;
}

template<typename T>
Array2D<T> operator* (const Array2D<T>& a, const Array2D<T>& b)
{
    Array2D<T> m(a);
    return (m *= b);
}

template<typename T>
Array2D<T> operator* (const Array2D<T>& a, const T& value)
{
    Array2D<T> m(a);
    return (m *= value);
}

template<typename T>
Array2D<T> operator* (const T& value, const Array2D<T>& a)
{
    Array2D<T> m(a);
    return (m *= value);
}

template<typename T>
ostream& operator<< (ostream& out, const Array2D<T>& a)
{
    for (int i = 0; i < a.get_rows(); i++) {
        for (int j = 0; j < a.get_cols(); j++)
            out << a.get_elem(j, i) << " ";
        out << endl;
    }
    return out;
}

template<typename T>
Array2D<T>& Array2D<T>::transpose()
{
    Array2D<T> m(*this);
    this->~Array2D();
    
    cols_ = m.get_rows();
    rows_ = m.get_cols();
    data_ = this->init_data(cols_, rows_, 0.);
    
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data_[i][j] = m.get_elem(j, i);

    return *this;
}

template<typename T>
Array2D<T>& Array2D<T>::swap_rows(unsigned row, unsigned target_row)
{
    for (int i = 0; i < cols_; i++)
        swap(data_[i][row], data_[i][target_row]);
    return *this;
}

template<typename T>
Array2D<T>& Array2D<T>::swap_cols(unsigned col, unsigned target_col)
{
    swap(data_[col], data_[target_col]);
    return *this;
}

template<typename T>
unsigned Array2D<T>::get_cols() const
{
    return cols_;
}

template<typename T>
unsigned Array2D<T>::get_rows() const
{
    return rows_;
}

template<typename T>
Array2D<T>& Array2D<T>::fill(T value)
{
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data_[i][j] = value;
    return *this; 
}

template<typename T>
Array2D<T>& Array2D<T>::zeros()
{
    return this->fill(0.);
}

template<typename T>
Array2D<T>& Array2D<T>::ones()
{
    return this->fill(1.);
}

template<typename T>
Array2D<T>& Array2D<T>::load(const string& input_file_name)
{
    ifstream fin;
    
    fin.open(input_file_name.c_str());
    if (!fin) {
        cerr << "An error occured while opening aray input file" << endl;
        exit(2);
    }
    
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            fin >> data_[j][i];
    fin.close();
}

template<typename T>
T** Array2D<T>::init_data(unsigned cols, unsigned rows, T initial_value)
{
    T** data = new T*[cols];

    for (int i = 0; i < cols; i++)
        data[i] = new T[rows];
    for (int i = 0; i < cols_; i++)
        for (int j = 0; j < rows_; j++)
            data[i][j] = initial_value;
        
    return data;
}

#endif	/* ARRAY2D_HPP */

