#ifndef MATRIX_EXCEPTION_HPP
#define	MATRIX_EXCEPTION_HPP

#include <exception>

using namespace std;

class MatrixException : public exception
{

    virtual const char* what() const throw()
    {
        return "ok";
    }

} me;

#endif	/* MATRIX_EXCEPTION_HPP */

