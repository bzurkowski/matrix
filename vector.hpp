#ifndef VECTOR_HPP
#define	VECTOR_HPP

#include "array2d.hpp"

using namespace std;
 
// friend functions pre-declaration
// ...

template<typename T>
class Vector : public Array2D<T>
{
    public:
        // constructors, destructors
        Vector(unsigned n);
        
        // elements access
        T& operator() (unsigned index);
        
        // operations
        
        // utils
        unsigned get_size();
};

template<typename T>
Vector<T>::Vector(unsigned n) : Array2D<T>(n, 1)
{
}

template<typename T>
T& Vector<T>::operator() (unsigned index)
{
    if (this->rows_ == 1) {
         if (index >= this->cols_) {
             cerr << "Vector index out of bounds" << endl;
             exit(2);
         }
         return this->data_[index][0];
    } else if (this->cols_ == 1) { // if transposed
         if (index >= this->rows_) {
             cerr << "Vector index out of bounds" << endl;
             exit(2);
         }
         return this->data_[0][index];
    }
}

template<typename T>
unsigned Vector<T>::get_size()
{
    if (this->rows_ == 1) {
        return this->cols_;
    } else if (this->cols_ == 1) { // if transposed
        return this->rows_;
    }  
}

#endif	/* VECTOR_HPP */

