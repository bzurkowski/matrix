#include <iostream>
#include <iomanip>
#include <ctime>

#include "matrix.hpp"
#include "vector.hpp"
#include "matrix_op.hpp"
#include "utils.hpp"

using namespace std;

template<typename T>
void exercise1(unsigned n, unsigned precision=6)
{
    Matrix<T> a(n, n);
    Vector<T> b(n);
    Vector<T> x1(n);
    Vector<T> x2(n);

    // fill matrix A
    a.fill(1.);
    for (int i = 1; i < n; i++)
        for (int j = 0; j < n; j++)
            a(i, j) = 1. / (i + j + 1.);
    
    // fill vector x1
    x1.transpose();
    for (int i = 0; i < n; i++) {
        if (i % 2 == 0)
            x1(i) = 1.;
        else
            x1(i) = -1.;
    }
    
    // calc vector b
    b = a * x1;
    
    
    // gauss
    x2 = MatrixOp<T>::gauss(a, b);
    
    // out
    //cout << a << endl;
    //cout << b << endl;
    //cout << x2 << endl;
    
    print_result<T>(diff_max(x1, x2), precision);
}

template<typename T>
void exercise2(unsigned n, unsigned precision=6)
{
    Matrix<T> a(n, n);
    Vector<T> b(n);
    Vector<T> x1(n);
    Vector<T> x2(n);

    // fill matrix A
    a.fill(1.);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (j >= i)
                a(i, j) = 1. * (i + 1) / (1. * (j + 1));
            else
                a(i, j) = a(j, i);
        }
    }
    
    // fill vector x1
    x1.transpose();
    for (int i = 0; i < n; i++) {
        if (i % 2 == 0)
            x1(i) = 1;
        else
            x1(i) = -1;
    }
    
    // calc vector b
    b = a * x1;
    
    // gauss
    x2 = MatrixOp<T>::gauss(a, b);
    
    // out
    //cout << a << endl;
    //cout << b << endl;
    //cout << x2 << endl;
    
    print_result<T>(diff_max<T>(x1, x2), precision);
}

template<typename T>
void exercise3a(unsigned n, T m, T k, unsigned precision=6)
{
    Matrix<T> a(n, n);
    Vector<T> b(n);
    Vector<T> x1(n);
    Vector<T> x2(n);
    
    // fill matrix A
    a.zeros();
    for (int i = 0; i < n; i ++)
        a(i, i) = k;
    for (int i = 0; i < n - 1; i ++)
        a(i, i + 1) = 1. / (i + m + 1.);
    for (int i = 1; i < n; i ++)
        a(i, i - 1) = 1. / (i + m + 2.);
    
    // fill vector x1
    x1.transpose();
    for (int i = 0; i < n; i++) {
        if (i % 2 == 0)
            x1(i) = 1.;
        else
            x1(i) = -1.;
    }
    
    // calc vector b
    b = a * x1;
    
    // thomas
    x2 = MatrixOp<T>::thomas(a, b);
    
    // out
    //cout << a << endl;
    //cout << b << endl;
    //cout << x2 << endl;
    
    print_result<T>(diff_max<T>(x1, x2), precision);
}

int main()
{
    
    //example();
    
    //for (int i = 0; i < 20; i++)
    //    exercise1<float>(i, 15);
    
    //exercise1<long double>(20, 15);
    
    cout << endl;
    
    //for (int i = 0; i < 50; i++)
    //    exercise2<double>(i, 15);
    
    //exercise2<double>(10, 15);
    //for (int i = 100; i < 1000; i += 100)
    //    exercise3a<float>(i, 5., 2., 15);
    
    exercise3a<float>(100, 3., 3., 15);

    
}