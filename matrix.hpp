#ifndef MATRIX_H
#define	MATRIX_H

#include "array2d.hpp"
#include "vector.hpp"

using namespace std;
 
// friend functions pre-declaration
template<typename T> class Matrix;
template<typename T> Vector<T> operator* (const Matrix<T>& a, const Vector<T>& b);
template<typename T> Vector<T> operator* (const Vector<T>& a, const Matrix<T>& b);

template<typename T>
class Matrix : public Array2D<T>
{
    public:
        //constructors, destructors
        Matrix(unsigned cols, unsigned rows);
        
        // operators
        Vector<T> operator*= (const Vector<T>& a);
        friend Vector<T> operator* <> (const Matrix<T>& a, const Vector<T>& b);
        friend Vector<T> operator* <> (const Vector<T>& a, const Matrix<T>& b);
};

template<typename T>
Matrix<T>::Matrix(unsigned cols, unsigned rows) : Array2D<T>(cols, rows) {}

template<typename T>
Vector<T> Matrix<T>::operator*= (const Vector<T>& a)
{
    if (a.get_rows() != this->cols_) {
        cerr << "Vector wrong dimensions or vector not transposed" << endl;
        exit(2);
    }
    
    Vector<T> v(this->rows_);
    v.transpose();
    
    for (int i = 0; i < this->rows_; i++)
        for (int j = 0; j < this->cols_; j++)
            v(i) += this->data_[j][i] * a.get_elem(0, j);

    return v;    
}

template<typename T>
Vector<T> operator* (const Matrix<T>& a, const Vector<T>& b)
{
    Matrix<T> m(a);
    return (m *= b);
}

template<typename T>
Vector<T> operator* (const Vector<T>& a, const Matrix<T>& b)
{
    Matrix<T> m(b);
    return (m *= b);
}


#endif	/* MATRIX_H */
